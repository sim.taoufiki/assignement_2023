package ma.octo.assignement.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import static org.assertj.core.api.Assertions.assertThat;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class TransferRepositoryTest {

	@Autowired
	private TransferRepository transferRepository;
	@Autowired
	private CompteRepository compteRepository;

	private Transfer transfer;

	@BeforeEach
	void init() {
		// initializes resources before all test methods
		Compte compteEmetteur = compteRepository.findByNrCompte("010000A000001000");
		Compte compteBeneficiaire = compteRepository.findByNrCompte("010000B025001000");
		transfer = new Transfer();
		transfer.setCompteEmetteur(compteEmetteur);
		transfer.setCompteBeneficiaire(compteBeneficiaire);
		transfer.setMontantTransfer(new BigDecimal(1000));
		transfer.setMotifTransfer("Transfer test");
		transfer.setDateExecution(new Date());
	}

	@Test
	public void findOne() {
		Transfer savedTransfer = transferRepository.save(transfer);
		Transfer transfer = transferRepository.findById(savedTransfer.getId()).get();
		Assertions.assertThat(transfer).isEqualTo(savedTransfer);
	}

	@Test
	public void findAll() {
		List<Transfer> transfers = transferRepository.findAll();
		Assertions.assertThat(transfers.size()).isGreaterThan(0);
	}

	@Test
	public void save() {
		Transfer savedTransfer = transferRepository.save(transfer);
		assertThat(savedTransfer).isNotNull();
		assertThat(transfer).isEqualTo(savedTransfer);
	}

	@Test
	public void delete() {
		Transfer savedTransfer = transferRepository.save(transfer);
		transferRepository.delete(savedTransfer);
		Optional<Transfer> optionalTransfer = transferRepository.findById(savedTransfer.getId());
		Transfer foundTransfer = null;
		if (optionalTransfer.isPresent()) {
			foundTransfer = optionalTransfer.get();
		}
		assertThat(foundTransfer).isNull();
	}
}