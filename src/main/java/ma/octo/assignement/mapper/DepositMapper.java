package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.responses.DepositResponse;

public class DepositMapper {
	private static DepositResponse depositResponse;

	public static DepositResponse map(MoneyDeposit moneyDeposit) {
		depositResponse = new DepositResponse();
		depositResponse.setNom_prenom_emetteur(moneyDeposit.getNom_prenom_emetteur());
		depositResponse.setDateExecution(moneyDeposit.getDateExecution());
		depositResponse.setMontant(moneyDeposit.getMontant());
		depositResponse.setMotifDeposit(moneyDeposit.getMotifDeposit());
		depositResponse.setCompteBeneficiaire(AccountResponseMapper.map(moneyDeposit.getCompteBeneficiaire()));
		return depositResponse;

	}
}
