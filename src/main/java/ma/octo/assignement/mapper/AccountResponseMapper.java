package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.responses.AccountResponse;

public class AccountResponseMapper {
	private static AccountResponse accountResponse;

    public static AccountResponse map(Compte compte) {
    	accountResponse = new AccountResponse();
        accountResponse.setNrCompte(compte.getNrCompte());
        accountResponse.setRib(compte.getRib());
        accountResponse.setSolde(compte.getSolde());
        accountResponse.setUtilisateur(UserResponseMapper.map(compte.getUtilisateur()));
        return accountResponse;

    }
}
