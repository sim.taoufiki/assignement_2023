package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.responses.TransferResponse;

public class TransferMapper {

    private static TransferResponse transferResponse;

    public static TransferResponse map(Transfer transfer) {
    	transferResponse = new TransferResponse();
        transferResponse.setMontantTransfer(transfer.getMontantTransfer());
        transferResponse.setDateExecution(transfer.getDateExecution());
        transferResponse.setCompteEmetteur(AccountResponseMapper.map(transfer.getCompteEmetteur()));
        transferResponse.setCompteBeneficiaire(AccountResponseMapper.map(transfer.getCompteBeneficiaire()));
        return transferResponse;

    }
}
