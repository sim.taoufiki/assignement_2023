package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.responses.UserResponse;

public class UserResponseMapper {
	private static UserResponse userResponse;

    public static UserResponse map(Utilisateur utilisateur) {
    	userResponse = new UserResponse();
        userResponse.setBirthdate(utilisateur.getBirthdate());
        userResponse.setFirstname(utilisateur.getFirstname());
        userResponse.setLastname(utilisateur.getLastname());
        userResponse.setGender(utilisateur.getGender());
        userResponse.setUsername(utilisateur.getUsername());
        return userResponse;

    }
}
