package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.errors.TransferErrorMessages;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.responses.TransferResponse;

@Service
public class TransferService {
	public static final int MONTANT_MAXIMAL = 10000;
	Logger LOGGER = LoggerFactory.getLogger(AuditService.class);
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private TransferRepository transferRepository;
	
	public List<TransferResponse> getAllTransfers() {
		LOGGER.info("Liste des transfers");
		var allTransferts = transferRepository.findAll();
		List<TransferResponse> listTransferts=new ArrayList<>();
		for(Transfer transfer: allTransferts) {
			TransferResponse transferResponse=TransferMapper.map(transfer);
			listTransferts.add(transferResponse);
		}
		return CollectionUtils.isEmpty(listTransferts) ? null : listTransferts;
	}
	
	
	public void addTransaction(TransferDto transferDto)
			throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
		Compte compteEmetteur = compteRepository.findByNrCompte(transferDto.getNrCompteEmetteur());
		Compte compteBeneficiaire = compteRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire());

		if (compteEmetteur == null) {
			throw new CompteNonExistantException("Compte Emetteur Non existant");
		}

		if (compteBeneficiaire == null) {
			throw new CompteNonExistantException("Compte Beneficiaire Non existant");
		}

		if (transferDto.getMontant().equals(null) || transferDto.getMontant().intValue() == 0) {
			throw new TransactionException(TransferErrorMessages.EMPTY_AMOUNT.getErrorMessage());
		} else if (transferDto.getMontant().intValue() < 10) {
			throw new TransactionException(TransferErrorMessages.AMOUNT_NOT_REACHED.getErrorMessage());
		} else if (transferDto.getMontant().intValue() > MONTANT_MAXIMAL) {
			throw new TransactionException(TransferErrorMessages.MINIMUM_AMOUNT_EXCEEDED.getErrorMessage());
		}

		if (transferDto.getMotif() == null || transferDto.getMotif().isBlank()) {
			throw new TransactionException(TransferErrorMessages.EMPTY_MOTIF.getErrorMessage());
		}

		if (compteEmetteur.getSolde().intValue() - transferDto.getMontant().intValue() < 0) {
			throw new SoldeDisponibleInsuffisantException("Pas de solde pas de transfer");
		}

		compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(transferDto.getMontant()));
		compteRepository.save(compteEmetteur);

		compteBeneficiaire.setSolde(
				new BigDecimal(compteBeneficiaire.getSolde().intValue() + transferDto.getMontant().intValue()));
		compteRepository.save(compteBeneficiaire);

		Transfer transfer = new Transfer();
		transfer.setDateExecution(transferDto.getDate());
		transfer.setCompteBeneficiaire(compteBeneficiaire);
		transfer.setCompteEmetteur(compteEmetteur);
		transfer.setMontantTransfer(transferDto.getMontant());

		transferRepository.save(transfer);
	}
}
