package ma.octo.assignement.service;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.mapper.AccountResponseMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.responses.AccountResponse;

@Service
public class CompteService {
	Logger LOGGER = LoggerFactory.getLogger(AuditService.class);
	@Autowired
	private CompteRepository compteRepository;
	public List<AccountResponse> getAllComptes() {
		LOGGER.info("Liste des comptes");
		List<Compte> allAccounts = compteRepository.findAll();
		List<AccountResponse> listAccounts=new ArrayList<>();
		for(Compte compte: allAccounts) {
			AccountResponse accountResponse=AccountResponseMapper.map(compte);
			listAccounts.add(accountResponse);
		}
		return CollectionUtils.isEmpty(listAccounts) ? null : listAccounts;
	}
}
