package ma.octo.assignement.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.mapper.UserResponseMapper;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.responses.UserResponse;

@Service
public class UserService {
	Logger LOGGER = LoggerFactory.getLogger(AuditService.class);
	@Autowired
	UtilisateurRepository utilisateurRepository;
	public List<UserResponse> getAllUtilisateur() {
		LOGGER.info("Liste des utilisateurs");
		List<Utilisateur> allUsers = utilisateurRepository.findAll();
		List<UserResponse> listUsers=new ArrayList<>();
		for(Utilisateur user: allUsers) {
			UserResponse userResponse=UserResponseMapper.map(user);
			listUsers.add(userResponse);
		}
		return CollectionUtils.isEmpty(listUsers) ? null : listUsers;
	}
}
