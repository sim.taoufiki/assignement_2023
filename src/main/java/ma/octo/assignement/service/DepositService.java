package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.errors.DepositErrorMessages;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.responses.DepositResponse;
import ma.octo.assignement.web.DepositController;

@Service
public class DepositService {
	public static final int MONTANT_MAXIMAL = 10000;
	Logger LOGGER = LoggerFactory.getLogger(DepositController.class);
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private DepositRepository depositRepository;

	public List<DepositResponse> getAllDeposits() {
		LOGGER.info("Liste des dépôts d'agent");
		var allDeposits = depositRepository.findAll();
		List<DepositResponse> listDeposits = new ArrayList<>();
		for (MoneyDeposit moneyDeposit : allDeposits) {
			DepositResponse depositResponse = DepositMapper.map(moneyDeposit);
			listDeposits.add(depositResponse);
		}
		return CollectionUtils.isEmpty(listDeposits) ? null : listDeposits;
	}

	public void deposit(DepositDto depositDto) throws CompteNonExistantException, DepositException {
		Compte compteBeneficiaire = compteRepository.findByRib(depositDto.getRib());
		if (compteBeneficiaire == null) {
			throw new CompteNonExistantException("Compte Beneficiaire Non existant");
		}
		if (depositDto.getMontant().equals(null) || depositDto.getMontant().intValue() == 0) {
			throw new DepositException(DepositErrorMessages.EMPTY_AMOUNT.getErrorMessage());
		} else if (depositDto.getMontant().intValue() < 10) {
			throw new DepositException(DepositErrorMessages.AMOUNT_NOT_REACHED.getErrorMessage());
		} else if (depositDto.getMontant().intValue() > MONTANT_MAXIMAL) {
			throw new DepositException(DepositErrorMessages.MINIMUM_AMOUNT_EXCEEDED.getErrorMessage());
		}
		if (depositDto.getMotifDeposit() == null || depositDto.getMotifDeposit().isBlank()) {
			throw new DepositException(DepositErrorMessages.EMPTY_MOTIF.getErrorMessage());
		}
		compteBeneficiaire.setSolde(
				new BigDecimal(compteBeneficiaire.getSolde().intValue() + depositDto.getMontant().intValue()));
		compteRepository.save(compteBeneficiaire);

		MoneyDeposit moneyDeposit = new MoneyDeposit();
		moneyDeposit.setDateExecution(depositDto.getDateExecution());
		moneyDeposit.setCompteBeneficiaire(compteBeneficiaire);
		moneyDeposit.setMontant(depositDto.getMontant());
		moneyDeposit.setNom_prenom_emetteur(depositDto.getNom_prenom_emetteur());
		moneyDeposit.setMotifDeposit(depositDto.getMotifDeposit());

		depositRepository.save(moneyDeposit);
	}
}
