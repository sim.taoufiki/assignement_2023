package ma.octo.assignement.web;

import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.responses.TransferResponse;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController(value = "/transfers")
class TransferController {
	@Autowired
	private TransferService transferService;
	@Autowired
	private AuditService auditService;

	@GetMapping("listOfTransfers")
	List<TransferResponse> loadAll() {
		return transferService.getAllTransfers();
	}

	@PostMapping("/executeTransfers")
	@ResponseStatus(HttpStatus.CREATED)
	public void createTransaction(@RequestBody TransferDto transferDto)
			throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
		transferService.addTransaction(transferDto);
		auditService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers "
				+ transferDto.getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant().toString());
	}
}
