package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.responses.UserResponse;
import ma.octo.assignement.service.UserService;

@RestController(value = "/utilisateurs")
public class UserController {
	@Autowired
	private UserService userService;
	
	
	@GetMapping("listOfUsers")
	List<UserResponse> loadAllUtilisateur() {
		return userService.getAllUtilisateur();
	}
}
