package ma.octo.assignement.web;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ma.octo.assignement.responses.AccountResponse;
import ma.octo.assignement.service.CompteService;

@RestController(value = "/comptes")
public class CompteController {
	@Autowired
	private CompteService compteService;
	
	@GetMapping("listOfAccounts")
	List<AccountResponse> loadAllCompte() {
		return compteService.getAllComptes();
	}
}
