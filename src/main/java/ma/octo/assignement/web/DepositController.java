package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;
import ma.octo.assignement.responses.DepositResponse;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.DepositService;

@RestController(value = "/deposits")
public class DepositController {
	@Autowired
	private DepositService depositService;
	@Autowired
	private AuditService auditService;

	@GetMapping("listOfDeposits")
	List<DepositResponse> loadAll() {
		return depositService.getAllDeposits();
	}
	
	@PostMapping("/deposit")
	@ResponseStatus(HttpStatus.CREATED)
	public void depositMoney(@RequestBody DepositDto depositDto)
			throws CompteNonExistantException,DepositException {
		
		depositService.deposit(depositDto);
		auditService.auditDeposit("Deposer par " + depositDto.getNom_prenom_emetteur() + " vers "
				+ depositDto.getRib() + " d'un montant de " + depositDto.getMontant().toString());
	}
}
