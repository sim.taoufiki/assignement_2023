package ma.octo.assignement.exceptions;

public class DepositException extends Exception {

	private static final long serialVersionUID = 9019507238229722860L;

	public DepositException() {
	}

	public DepositException(String message) {
		super(message);
	}
}
