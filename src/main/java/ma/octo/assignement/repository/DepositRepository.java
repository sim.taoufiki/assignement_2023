package ma.octo.assignement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.domain.MoneyDeposit;

public interface DepositRepository extends JpaRepository<MoneyDeposit, Long> {

}
