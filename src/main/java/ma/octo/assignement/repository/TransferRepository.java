package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Transfer;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TransferRepository extends JpaRepository<Transfer, Long> {
	Optional<Transfer> findById(Long id);
}
