package ma.octo.assignement.responses;

import java.math.BigDecimal;
import java.util.Date;

public class DepositResponse {
	private BigDecimal Montant;
	private Date dateExecution;
	private String nom_prenom_emetteur;
	private AccountResponse compteBeneficiaire;
	private String motifDeposit;

	public BigDecimal getMontant() {
		return Montant;
	}

	public void setMontant(BigDecimal montant) {
		Montant = montant;
	}

	public Date getDateExecution() {
		return dateExecution;
	}

	public void setDateExecution(Date dateExecution) {
		this.dateExecution = dateExecution;
	}

	public String getNom_prenom_emetteur() {
		return nom_prenom_emetteur;
	}

	public void setNom_prenom_emetteur(String nom_prenom_emetteur) {
		this.nom_prenom_emetteur = nom_prenom_emetteur;
	}

	public AccountResponse getCompteBeneficiaire() {
		return compteBeneficiaire;
	}

	public void setCompteBeneficiaire(AccountResponse compteBeneficiaire) {
		this.compteBeneficiaire = compteBeneficiaire;
	}

	public String getMotifDeposit() {
		return motifDeposit;
	}

	public void setMotifDeposit(String motifDeposit) {
		this.motifDeposit = motifDeposit;
	}

}
