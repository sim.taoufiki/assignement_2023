package ma.octo.assignement.responses;

import java.math.BigDecimal;

public class AccountResponse {
	private String nrCompte;
	private String rib;
	private BigDecimal solde;
	private UserResponse utilisateur;

	public String getNrCompte() {
		return nrCompte;
	}

	public void setNrCompte(String nrCompte) {
		this.nrCompte = nrCompte;
	}

	public String getRib() {
		return rib;
	}

	public void setRib(String rib) {
		this.rib = rib;
	}

	public BigDecimal getSolde() {
		return solde;
	}

	public void setSolde(BigDecimal solde) {
		this.solde = solde;
	}

	public UserResponse getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(UserResponse utilisateur) {
		this.utilisateur = utilisateur;
	}

}
