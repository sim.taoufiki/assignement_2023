package ma.octo.assignement.responses;

import java.math.BigDecimal;
import java.util.Date;

public class TransferResponse {
	private BigDecimal montantTransfer;
	private Date dateExecution;
	private AccountResponse compteEmetteur;
	private AccountResponse compteBeneficiaire;

	public BigDecimal getMontantTransfer() {
		return montantTransfer;
	}

	public void setMontantTransfer(BigDecimal montantTransfer) {
		this.montantTransfer = montantTransfer;
	}

	public Date getDateExecution() {
		return dateExecution;
	}

	public void setDateExecution(Date dateExecution) {
		this.dateExecution = dateExecution;
	}

	public AccountResponse getCompteEmetteur() {
		return compteEmetteur;
	}

	public void setCompteEmetteur(AccountResponse compteEmetteur) {
		this.compteEmetteur = compteEmetteur;
	}

	public AccountResponse getCompteBeneficiaire() {
		return compteBeneficiaire;
	}

	public void setCompteBeneficiaire(AccountResponse compteBeneficiaire) {
		this.compteBeneficiaire = compteBeneficiaire;
	}

}
