package ma.octo.assignement.errors;

public enum TransferErrorMessages {
	
	EMPTY_AMOUNT("Montant vide."),
    AMOUNT_NOT_REACHED("Montant minimal de transfer non atteint."),
    MINIMUM_AMOUNT_EXCEEDED("Montant maximal de transfer dépassé."),
    EMPTY_MOTIF("Motif vide.");
	
	private String errorMessage;

	private TransferErrorMessages(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	 
	 
}