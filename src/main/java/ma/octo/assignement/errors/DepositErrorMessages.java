package ma.octo.assignement.errors;

public enum DepositErrorMessages {
	EMPTY_AMOUNT("Montant vide pour le dépôt."), 
	AMOUNT_NOT_REACHED("Montant minimal pour deposé non atteint."),
	MINIMUM_AMOUNT_EXCEEDED("Montant maximum du dépôt dépassé."), 
	EMPTY_MOTIF("Motif vide veuillez specifier.");

	private String errorMessage;

	private DepositErrorMessages(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
